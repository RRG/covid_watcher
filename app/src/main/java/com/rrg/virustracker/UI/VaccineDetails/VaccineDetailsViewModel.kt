package com.rrg.virustracker.UI.VaccineDetails

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import com.rrg.virustracker.Dependancy_inj.ApplicationScope
import com.rrg.virustracker.data.Repository
import kotlinx.coroutines.CoroutineScope

class VaccineDetailsViewModel @ViewModelInject constructor(
    private val repository: Repository,
    @ApplicationScope private val app_scope: CoroutineScope
) : ViewModel() {


}