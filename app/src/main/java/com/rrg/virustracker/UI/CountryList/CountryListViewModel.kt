package com.rrg.virustracker.UI.CountryList

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.rrg.virustracker.Dependancy_inj.ApplicationScope
import com.rrg.virustracker.UI.BaseViewModel
import com.rrg.virustracker.data.Country_data
import com.rrg.virustracker.data.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.util.*


class CountryListViewModel @ViewModelInject constructor(
    private val repository: Repository,
    @ApplicationScope private val app_scope: CoroutineScope
) : BaseViewModel() {

    private val currentQuery = MutableLiveData(DEFAULT_QUERY)

    //val results = repository.getSearchResults("")
/*
    val records = currentQuery.switchMap { query ->
        repository.getSearchResults(query).cachedIn(viewModelScope)
    }*/

    private val list= MutableLiveData<List<Country_data>>()

    val final_list: LiveData<List<Country_data>> = list

    fun getEverything() = viewModelScope.launch {
        list.value = repository.getSearchResults_new()
    }


    fun getAllCountriesFiltered(query: CharSequence) = viewModelScope.launch {
        if (query.isNotBlank()) {
            list.value = repository.getSearchResults_new().filter {
                it.country.toLowerCase(Locale.ROOT).startsWith(query.toString().toLowerCase(Locale.ROOT))
            }
        }else{
            list.value = repository.getSearchResults_new()
        }
        _isLoading.value = false
    }


    /*
    fun records_new() :LiveData<List<Country_data>> {
         repository.getSearchResults_new(DEFAULT_QUERY)
    }
    */

    val _isLoading = MutableLiveData(true)

    companion object{
        private const val DEFAULT_QUERY = "Malaysia" //empty query means all countries
    }
}