package com.rrg.virustracker.UI.VaccineList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rrg.virustracker.data.Vaccines
import com.rrg.virustracker.databinding.VaccineRecordBinding

class VaccineAdapter(private val listener:onClickListener) :
    ListAdapter<Vaccines.Vaccine, VaccineAdapter.VaccineViewHolder>(DiffCallback()) {

    interface onClickListener {
        fun onVaccineClicked(vaccine: Vaccines.Vaccine)
    }


    inner class VaccineViewHolder(
        private val binding: VaccineRecordBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.apply {
                root.setOnClickListener {
                    val index = bindingAdapterPosition
                    if (index != RecyclerView.NO_POSITION) {//invalid position --> -1
                        val vaccine = getItem(index)
                        if (vaccine != null) {
                            listener.onVaccineClicked(vaccine)
                        }
                    }
                }
            }

        }

        fun bind(record: Vaccines.Vaccine) {
            binding.apply {
                tvVaccineCandidate.text = record.candidate
                tvVaccineCandidate.isSelected = true
                tvVaccineSponsor.text = record.sponsors[0]
                tvVaccineSponsor.isSelected = true
                tvVaccineMechanism.text = record.mechanism
                tvVaccineMechanism.isSelected = true
            }
        }
    }

    class DiffCallback : DiffUtil.ItemCallback<Vaccines.Vaccine>() {
        override fun areItemsTheSame(
            oldItem: Vaccines.Vaccine,
            newItem: Vaccines.Vaccine
        ): Boolean {
            return oldItem.candidate == newItem.candidate
        }

        override fun areContentsTheSame(
            oldItem: Vaccines.Vaccine,
            newItem: Vaccines.Vaccine
        ): Boolean {
            return oldItem == newItem
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VaccineViewHolder {
        val binding = VaccineRecordBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return VaccineViewHolder(binding)
    }

    override fun onBindViewHolder(holder: VaccineViewHolder, position: Int) {
        val current_item = getItem(position)
        holder.bind(current_item!!)
    }

}


