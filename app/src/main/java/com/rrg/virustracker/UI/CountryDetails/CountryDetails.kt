package com.rrg.virustracker.UI.CountryDetails

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.rrg.virustracker.R
import com.rrg.virustracker.UI.Home.LandingPageDirections
import com.rrg.virustracker.databinding.CountryDetailsBinding
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

@AndroidEntryPoint
class CountryDetails : Fragment(R.layout.country_details) {

    private val viewModel by viewModels<CountryDetailsViewModel>()

    private val args by navArgs<CountryDetailsArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = CountryDetailsBinding.bind(view)

        binding.apply {
            val country = args.country

            Glide.with(this@CountryDetails)
                .load(country.countryInfo.flag)
                .error(R.drawable.ic_baseline_error_24)
                .circleCrop()
                .into(ivDetailsFlag)

            tvDetailsName.text = country.country
            tvDetailsContinent.text = country.continent

            textAnimation(country.cases,tvTotalAffected)
            textAnimation(country.active,tvDetailActive)
            textAnimation(country.recovered,tvDetailRecovered)
            textAnimation(country.deaths,tvDetailDeaths)

            tvActiveToday.text = "+"+formatNumbers(country.todayCases.toString())
            tvRecoveredToday.text = "+"+formatNumbers(country.todayRecovered.toString())
            tvDeathsToday.text = "+"+formatNumbers(country.todayDeaths.toString())

            val extra_text: String =
                resources.getString(R.string.statement_detail,
                    country.country,
                    country.casesPerOneMillion.toInt(),
                    country.deathsPerOneMillion.toInt())
            tvDetailsExtraInfo.text = extra_text

            tvDetailsDate.text = "Updated on "+ SimpleDateFormat("dd/MM/YYYY",
                Locale.getDefault()).format(country.updated)

            btnBackDetails.setOnClickListener {
                findNavController().navigateUp()
            }

            btnCountryHistory.setOnClickListener {
                val action = CountryDetailsDirections.actionCountryDetailsToCountryHistory(country.country)
                findNavController().navigate(action)
            }
        }
    }

    private fun textAnimation(finalVal: Int, textView: TextView){
        val valueAnimator = ValueAnimator.ofInt(0, finalVal)
        valueAnimator.duration = 1500
        valueAnimator.addUpdateListener { valueAnimator -> textView.text = formatNumbers(valueAnimator.animatedValue.toString()) }
        valueAnimator.start()
    }

    private fun formatNumbers(number :String): String {
        val number_int = number.toInt()
        return NumberFormat.getNumberInstance(Locale.getDefault()).format(number_int).toString()
    }

}