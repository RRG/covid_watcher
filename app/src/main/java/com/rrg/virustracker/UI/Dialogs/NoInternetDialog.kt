package com.rrg.virustracker.UI.Dialogs

import android.app.AlertDialog
import android.app.Dialog
import android.os.Bundle
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.viewModels
import com.rrg.virustracker.R
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class NoInternetDialog:DialogFragment() {

    private val viewModel: NoInternetDialogViewModel by viewModels()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
        builder.setTitle("No Internet Connection")
        builder.setMessage("This app cannot run without the internet")
        isCancelable = false
        builder.setNeutralButton("Ok"){ _,_->
            viewModel.onClick()
        }
        return builder.create()
    }
}