package com.rrg.virustracker.UI.VaccineList

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rrg.virustracker.R
import com.rrg.virustracker.UI.Home.LandingPageDirections
import com.rrg.virustracker.data.Vaccines
import com.rrg.virustracker.databinding.HomePageBinding
import com.rrg.virustracker.databinding.VaccineListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class VaccineList: Fragment(R.layout.vaccine_list), VaccineAdapter.onClickListener {

    private val viewModel by viewModels<VaccineListViewModel>()
    private var _binding: VaccineListBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = VaccineListBinding.bind(view)
        val adapter = VaccineAdapter(this)
        val response = viewModel.getConnectionType(this.requireContext())

        val binding_loading = HomePageBinding.bind(view)

        binding_loading.apply {
            viewModel._isLoading.observe(viewLifecycleOwner){
                when(it){
                    false -> loadingScreen.visibility = View.GONE
                    true ->loadingScreen.visibility - View.GONE
                }
            }
        }

        binding.apply {
            rvVaccineList.layoutManager= LinearLayoutManager(requireContext())
            rvVaccineList.setHasFixedSize(true)
            rvVaccineList.adapter = adapter
        }

        if (response == 0){
            val action = LandingPageDirections.actionGlobalNoInternetDialog()
            findNavController().navigate(action)

        }else{
            viewModel.getVaccineList()
        }

        viewModel.final_vaccine_data.observe(viewLifecycleOwner) {
            adapter.submitList(it.data)
        }

    }




    override fun onVaccineClicked(vaccine: Vaccines.Vaccine) {
       val action = VaccineListDirections.actionNavigationVaccineToVaccineDetails(vaccine)
        findNavController().navigate(action)
    }

}