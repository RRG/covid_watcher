package com.rrg.virustracker.UI.Home

import android.animation.ValueAnimator
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.rrg.virustracker.R
import com.rrg.virustracker.databinding.HomePageBinding
import com.rrg.virustracker.databinding.HomePageContentBinding
import dagger.hilt.android.AndroidEntryPoint
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*


@AndroidEntryPoint
class LandingPage: Fragment(R.layout.home_page) {

    private val viewModel by viewModels<LandingPageViewModel>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val response  = viewModel.getConnectionType(this.requireContext())


        val binding = HomePageContentBinding.bind(view)
        val binding_loading = HomePageBinding.bind(view)

        if (response == 0){
            val action = LandingPageDirections.actionGlobalNoInternetDialog()
            findNavController().navigate(action)

        }else{
            viewModel.getGlobalData()
        }


        binding_loading.apply {
            viewModel._isLoading.observe(viewLifecycleOwner){
                when(it){
                    false -> loadingScreen.visibility = View.GONE
                    true ->loadingScreen.visibility - View.GONE
                }
            }
        }

        binding.apply {
            viewModel.global_data.observe(viewLifecycleOwner) {

                textAnimation(it.active, tvActiveCasesGlobal)
                textAnimation(it.recovered, tvRecoveredCasesGlobal)
                textAnimation(it.deaths, tvDeathCasesGlobal)
                tvActiveToday.text = "+"+formatNumbers(it.todayCases.toString())
                tvRecoveredToday.text = "+"+formatNumbers(it.todayRecovered.toString())
                tvDeathsToday.text = "+"+formatNumbers(it.todayDeaths.toString())
                tvDate.text = "Updated on "+ SimpleDateFormat("dd/MM/YYYY",
                    Locale.getDefault()).format(it.updated)

                val extra_text: String =
                    resources.getString(R.string.statement,
                        it.casesPerOneMillion.toInt(),
                        it.deathsPerOneMillion.toInt(),
                        it.affectedCountries)
                tvExtraInfo.text = extra_text
            }


            btnMap.setOnClickListener {
                Toast.makeText(context,"Coming Soon !",Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun textAnimation(finalVal: Int, textView: TextView){
        val valueAnimator = ValueAnimator.ofInt(0, finalVal)
        valueAnimator.duration = 1500
        valueAnimator.addUpdateListener { valueAnimator -> textView.text = formatNumbers(valueAnimator.animatedValue.toString()) }
        valueAnimator.start()
    }

    private fun formatNumbers(number :String): String {
        val number_int = number.toInt()
        return NumberFormat.getNumberInstance(Locale.getDefault()).format(number_int).toString()
    }



}