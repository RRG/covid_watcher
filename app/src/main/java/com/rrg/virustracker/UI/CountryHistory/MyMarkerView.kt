package com.rrg.virustracker.UI.CountryHistory

import android.content.Context;
import android.view.View
import android.widget.TextView;

import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.rrg.virustracker.R
import java.text.NumberFormat
import java.util.*


class MyMarkerView(context: Context?, layoutResource: Int, labels: MutableList<String>)
    : MarkerView(context, layoutResource) {

    private val cases: TextView = findViewById<View>(R.id.tv_graphPointContent1) as TextView
    private val date: TextView = findViewById<View>(R.id.tv_graphPointContent2) as TextView
    private val date_range = labels

    // callbacks everytime the MarkerView is redrawn, can be used to update the
    // content (user-interface)
    override fun refreshContent(e: Entry, highlight: Highlight?) {
        cases.text = "+" + formatNumbers(e.y.toInt().toString())
        date.text = "" + date_range[e.x.toInt()]
        super.refreshContent(e, highlight)
    }

    override fun getOffset(): MPPointF {
        return MPPointF((-(width / 2)).toFloat(), (-height).toFloat()-50)
    }

    private fun formatNumbers(number :String): String {
        val number_int = number.toInt()
        return NumberFormat.getNumberInstance(Locale.getDefault()).format(number_int).toString()
    }
}