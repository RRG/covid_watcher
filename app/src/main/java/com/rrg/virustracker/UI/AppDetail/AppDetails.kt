package com.rrg.virustracker.UI.AppDetail

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rrg.virustracker.R
import com.rrg.virustracker.databinding.AppDetailsBinding

class AppDetails : Fragment(R.layout.app_details) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = AppDetailsBinding.bind(view)


        binding.apply {
            btnBackAppDetails.setOnClickListener {
                findNavController().navigateUp()
            }
        }
    }

}