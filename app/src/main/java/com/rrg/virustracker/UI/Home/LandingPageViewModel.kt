package com.rrg.virustracker.UI.Home

import android.content.Context
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.rrg.virustracker.UI.BaseViewModel
import com.rrg.virustracker.data.Country_data
import com.rrg.virustracker.data.Repository
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.launch

class LandingPageViewModel @ViewModelInject constructor(
    private val repository: Repository,
    @ApplicationContext private val context: Context
) : BaseViewModel() {

    private val list= MutableLiveData<Country_data>()

    val global_data: LiveData<Country_data> = list

    fun getGlobalData() = viewModelScope.launch {
        list.value = repository.getGlobalData()
        _isLoading.value = false
    }


    val _isLoading = MutableLiveData(true)


}