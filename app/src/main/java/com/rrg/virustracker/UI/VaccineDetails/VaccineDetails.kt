package com.rrg.virustracker.UI.VaccineDetails

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.rrg.virustracker.R
import com.rrg.virustracker.databinding.VaccineDetailsBinding

class VaccineDetails : Fragment(R.layout.vaccine_details) {

    private val viewModel by viewModels<VaccineDetailsViewModel>()

    private val args by navArgs<VaccineDetailsArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val binding = VaccineDetailsBinding.bind(view)
        val vaccine  = args.Vaccine

        binding.apply {
            btnBackDetails.setOnClickListener {
                findNavController().navigateUp()
            }
            tvVaccineCandidateDetails.text =vaccine.candidate
            tvVaccineCandidateDetails.isSelected = true
            tvVaccineMechanismDetails.text = vaccine.mechanism
            tvVaccineMechanismDetails.isSelected = true
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tvVaccineDescription.text =
                    Html.fromHtml(vaccine.details,
                        Html.FROM_HTML_MODE_COMPACT);
            } else {
                tvVaccineDescription.text =
                    Html.fromHtml(vaccine.details);
            }
            //tvVaccineDescription.text = vaccine.details
        }
    }
}