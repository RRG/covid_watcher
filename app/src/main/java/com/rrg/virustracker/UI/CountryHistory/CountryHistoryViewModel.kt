package com.rrg.virustracker.UI.CountryHistory

import android.util.Log
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.rrg.virustracker.Dependancy_inj.ApplicationScope
import com.rrg.virustracker.UI.BaseViewModel
import com.rrg.virustracker.data.Country_data
import com.rrg.virustracker.data.Historical_data
import com.rrg.virustracker.data.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class CountryHistoryViewModel  @ViewModelInject constructor(
    private val repository: Repository,
    @ApplicationScope private val app_scope: CoroutineScope
) : BaseViewModel() {

    private val list= MutableLiveData<Historical_data>()

    val final_historical_data: LiveData<Historical_data> = list

    fun getHistoricalData(country:String) = viewModelScope.launch {
        try {
            list.value = repository.getHistoricalData(country)
        }catch (e: Exception){
            Log.i("ass","hehe error")
            _isLoading.value = false
        }
        _isLoading.value = false
    }


    val _isLoading = MutableLiveData(true)

}