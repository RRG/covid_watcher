package com.rrg.virustracker.UI.VaccineList

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.rrg.virustracker.UI.BaseViewModel
import com.rrg.virustracker.data.Repository
import com.rrg.virustracker.data.Vaccines
import kotlinx.coroutines.launch

class VaccineListViewModel @ViewModelInject constructor(
    private val repository: Repository
) : BaseViewModel() {

    private val list= MutableLiveData<Vaccines>()

    val final_vaccine_data: LiveData<Vaccines> = list

    fun getVaccineList() = viewModelScope.launch {
        list.value = repository.getVaccineData()
        _isLoading.value = false
    }

    val _isLoading = MutableLiveData(true)


}