package com.rrg.virustracker.UI.CountryList

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterInside
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.rrg.virustracker.R
import com.rrg.virustracker.data.Country_data
import com.rrg.virustracker.databinding.CountryRecordBinding


class CountryAdapter(private val listener:onClickListener) :
    ListAdapter<Country_data, CountryAdapter.CountryViewHolder>(DiffCallback()) {

    interface onClickListener{
        fun onCountryClicked(country:Country_data)
        fun onStarredClicked(country_name:String)
    }


    inner class CountryViewHolder(
        private val binding: CountryRecordBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.apply {
                root.setOnClickListener{
                    val index = bindingAdapterPosition
                    if(index != RecyclerView.NO_POSITION){//invalid position --> -1
                        val country  = getItem(index)
                        if (country != null){
                            listener.onCountryClicked(country)
                        }
                    }
                }

                ivStarred.setOnClickListener {
                    val index = bindingAdapterPosition
                    if (index != RecyclerView.NO_POSITION) {
                        //clicking star
                        val country  = getItem(index)
                        if (country != null){
                            listener.onStarredClicked(country.country)
                        }
                    }
                }
            }

        }

        fun bind(record: Country_data){
            binding.apply {
                Glide.with(itemView).load(record.countryInfo.flag)
                    .centerCrop()
                    .transform(CenterInside(),RoundedCorners(10))
                    .apply(RequestOptions().placeholder(R.drawable.ic_baseline_error_24)
                            .error(R.drawable.ic_baseline_error_24))
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .error(R.drawable.ic_baseline_error_24)
                    .into(ivFlagImg)
                tvCountryName.text = record.country
                tvCountryName.isSelected = true
                tvContinent.text = record.continent
                tvContinent.isSelected = true
            }
        }


    }

    class DiffCallback : DiffUtil.ItemCallback<Country_data>() {
        override fun areItemsTheSame(oldItem: Country_data, newItem: Country_data): Boolean {
            return oldItem.countryInfo.iso3 == newItem.countryInfo.iso3
        }

        override fun areContentsTheSame(oldItem: Country_data, newItem: Country_data): Boolean {
            return oldItem == newItem
        }


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CountryViewHolder {
        val binding = CountryRecordBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return CountryViewHolder(binding)
    }

    override fun onBindViewHolder(holder: CountryViewHolder, position: Int) {
        val current_item = getItem(position)
        holder.bind(current_item!!)
    }
    }


