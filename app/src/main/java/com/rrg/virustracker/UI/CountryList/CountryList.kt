package com.rrg.virustracker.UI.CountryList

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.rrg.virustracker.R
import com.rrg.virustracker.UI.Home.LandingPageDirections
import com.rrg.virustracker.data.Country_data
import com.rrg.virustracker.databinding.CountryListBinding
import com.rrg.virustracker.databinding.HomePageBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CountryList :Fragment(R.layout.country_list),CountryAdapter.onClickListener {

    private val viewModel by viewModels<CountryListViewModel>()
    private var _binding : CountryListBinding? = null
    private val binding get() = _binding!!

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        _binding = CountryListBinding.bind(view)
        val adapter = CountryAdapter(this)
        val response  = viewModel.getConnectionType(this.requireContext())

        val binding_loading = HomePageBinding.bind(view)

        binding_loading.apply {
            viewModel._isLoading.observe(viewLifecycleOwner){
                when(it){
                    false -> loadingScreen.visibility = View.GONE
                    true ->loadingScreen.visibility - View.GONE
                }
            }
        }

        binding.apply {
            rvCountryList.layoutManager = StaggeredGridLayoutManager(3,StaggeredGridLayoutManager.VERTICAL)
            rvCountryList.setHasFixedSize(true)
            rvCountryList.adapter = adapter

            etCountry.addTextChangedListener(object : TextWatcher {

                override fun afterTextChanged(s: Editable) {}

                override fun beforeTextChanged(s: CharSequence, start: Int,
                                               count: Int, after: Int) {
                }

                override fun onTextChanged(s: CharSequence, start: Int,
                                           before: Int, count: Int) {

                    if (response == 0){
                        val action = LandingPageDirections.actionGlobalNoInternetDialog()
                        findNavController().navigate(action)

                    }else{
                        viewModel.getAllCountriesFiltered(s)
                    }
                }
            })

        }


        if (response == 0){
            val action = LandingPageDirections.actionGlobalNoInternetDialog()
            findNavController().navigate(action)

        }else{
            viewModel.getAllCountriesFiltered("")
        }

        viewModel.final_list.observe(viewLifecycleOwner) {
            adapter.submitList(it)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCountryClicked(country: Country_data) {
        val action = CountryListDirections.actionMainPageToCountryDetails(country)
        findNavController().navigate(action)
    }

    override fun onStarredClicked(country_name: String) {
        //save country name
    }
}