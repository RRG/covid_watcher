package com.rrg.virustracker.UI.CountryHistory

import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import com.github.mikephil.charting.utils.Utils
import com.rrg.virustracker.R
import com.rrg.virustracker.UI.CountryDetails.CountryDetailsDirections
import com.rrg.virustracker.UI.Home.LandingPageDirections
import com.rrg.virustracker.databinding.CountryHistoryBinding
import com.rrg.virustracker.databinding.CountryListBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.country_history.*
import java.util.*


@AndroidEntryPoint
class CountryHistory: Fragment(R.layout.country_history) {

    companion object{
        const val CASES="cases"
        const val DEATHS = "deaths"
        const val RECOVERED = "recovered"
    }

    private val viewModel by viewModels<CountryHistoryViewModel>()

    private val args by navArgs<CountryHistoryArgs>()
    private var _binding : CountryHistoryBinding? = null
    private val binding get() = _binding!!


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = CountryHistoryBinding.bind(view)
        //graph data
        val labels = mutableListOf<String>()
        val data_cases = ArrayList<Entry>()
        val data_recovered = ArrayList<Entry>()
        val data_deaths = ArrayList<Entry>()

        val response  = viewModel.getConnectionType(this.requireContext())

        if (response == 0){ //no internet
            val action = LandingPageDirections.actionGlobalNoInternetDialog()
            findNavController().navigate(action)

        }else{
            viewModel.getHistoricalData(args.countryName)
        }


        viewModel.final_historical_data.observe(viewLifecycleOwner) {
            Log.i("ass", it.timeline.cases.toString())

            viewModel._isLoading.observe(viewLifecycleOwner){
                when(it){
                    false -> loading_screen_history.visibility = View.GONE
                    true ->loading_screen_history.visibility - View.GONE
                }
            }

            var prev = 0
            var count = 0f
            for ((key, value) in it.timeline.cases){
                if(count >= 1){
                    labels.add(fixDateFormat(key))
                    data_cases.add(Entry(count - 1, value.toFloat() - prev))
                }
                prev = value
                count++
            }

            prev = 0
            count = 0f
            for ((_, value) in it.timeline.deaths){
                if(count >= 1){
                    data_deaths.add(Entry(count - 1, value.toFloat() - prev))
                }
                prev = value
                count++
            }

            prev = 0
            count = 0f
            for ((_, value) in it.timeline.recovered){
                if(count >= 1){
                    data_recovered.add(Entry(count - 1, value.toFloat() - prev))
                }
                prev = value
                count++
            }


            binding.apply {
                lineChart.data = chartDataInit(data_cases, CASES)
                lineChart.invalidate();
                lineChart.refreshDrawableState();

                lineChart.setScaleEnabled(false)
                lineChart.isScaleYEnabled = false
                lineChart.isScaleXEnabled = false

                lineChart.marker = MyMarkerView(requireContext(),R.layout.graph_marker,labels)
                lineChart.axisLeft.textColor = Color.WHITE
                lineChart.axisLeft.setDrawGridLines(false)
                lineChart.axisRight.setDrawGridLines(false)
                //lineChart.axisRight.setDrawLabels(false)
                //lineChart.setViewPortOffsets(100f,100f,100f,100f)
                val xAxis = binding.lineChart.xAxis
                xAxis.position = XAxis.XAxisPosition.BOTTOM;
                xAxis.setDrawAxisLine(true);
                xAxis.setDrawGridLines(false);
                xAxis.isGranularityEnabled = true;
                xAxis.granularity = 5f
                xAxis.setDrawLabels(true);
                xAxis.labelCount = labels.size // important
                xAxis.textColor = Color.WHITE

                xAxis.valueFormatter = object : ValueFormatter() {
                    override
                    fun getFormattedValue(value: Float): String {
                        // value is x as index
                        return labels[value.toInt()]
                    }
                }

                lineChart.axisRight.textColor = Color.WHITE
                lineChart.legend.textColor = Color.WHITE
                lineChart.setDrawBorders(false);

                //padding
                lineChart.extraLeftOffset=1f
                lineChart.extraRightOffset=1f
                // hide legend
                lineChart.legend.isEnabled = false;
                // no description text
                lineChart.description.isEnabled = false;

                tvGraphCountryName.text=args.countryName;

                btnBackGraph.setOnClickListener {
                    findNavController().navigateUp()
                }

                btnDailyCases.setOnClickListener {
                    lineChart.data = chartDataInit(data_cases, CASES)
                    lineChart.invalidate();
                    lineChart.refreshDrawableState();
                    lineChart.notifyDataSetChanged()
                }

                btnDailyDeaths.setOnClickListener {
                    lineChart.data = chartDataInit(data_deaths, DEATHS)
                    lineChart.invalidate();
                    lineChart.refreshDrawableState();
                    lineChart.notifyDataSetChanged()
                }

                btnDailyRecovered.setOnClickListener {
                    lineChart.data = chartDataInit(data_recovered, RECOVERED)
                    lineChart.invalidate();
                    lineChart.refreshDrawableState();
                    lineChart.notifyDataSetChanged()
                }

            }


        }
    }


    private fun chartDataInit(data: ArrayList<Entry>, data_type: String): LineData {
        val line_dataset = LineDataSet(data, "dataset 1")
        line_dataset.setDrawFilled(true)
        line_dataset.mode = LineDataSet.Mode.CUBIC_BEZIER
        line_dataset.lineWidth = 2f;
        line_dataset.color = Color.BLACK;
        line_dataset.setDrawCircles(false)
        line_dataset.setDrawValues(false)

        if (Utils.getSDKInt() >= 18) {
            // fill drawable only supported on api level 18 and above
            var drawable = ContextCompat.getDrawable(requireContext(), R.drawable.fade_yellow)
            when(data_type){
                CASES ->{
                    drawable = ContextCompat.getDrawable(requireContext(), R.drawable.fade_yellow)
                }
                DEATHS ->{
                    drawable = ContextCompat.getDrawable(requireContext(), R.drawable.fade_red)
                }
                RECOVERED->{
                    drawable = ContextCompat.getDrawable(requireContext(), R.drawable.fade_green)
                }
            }
            line_dataset.fillDrawable = drawable
        } else {
            line_dataset.fillColor = Color.BLACK
        }
        val dataset = ArrayList<ILineDataSet>()
        dataset.add(line_dataset)
        var chartdata = LineData(dataset)
        return chartdata;
    }


    private fun fixDateFormat(date: String): String{
        val decomposed = date.split('/')
        return decomposed[1]+"/"+decomposed[0]+"/"+decomposed[2]
    }
}