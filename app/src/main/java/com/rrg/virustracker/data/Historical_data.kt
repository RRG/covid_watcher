package com.rrg.virustracker.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*


@Parcelize
data class Historical_data(
    val country: String,
    val province: List<String>,
    val timeline: Description
): Parcelable {

    @Parcelize
    data class Description(
        val cases: Map<String, Int>,
        val deaths: Map<String,Int>,
        val recovered: Map<String,Int>
    ): Parcelable
}