package com.rrg.virustracker.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Country_data(
        val updated: Long,
        val country: String,
        val countryInfo: CountryInfoData,
        val continent: String,
        val cases: Int,
        val deaths: Int,
        val recovered: Int,
        val todayCases: Int,
        val todayDeaths: Int,
        val todayRecovered: Int,
        val active: Int,
        val critical: Int,
        val casesPerOneMillion: Double,
        val deathsPerOneMillion: Double,
        val affectedCountries: Int
    ) : Parcelable {

        @Parcelize
        data class CountryInfoData(
            val iso2: String,
            val iso3: String,
            val lat: Double,
            val long: Double,
            val flag: String
        ) : Parcelable
    }
