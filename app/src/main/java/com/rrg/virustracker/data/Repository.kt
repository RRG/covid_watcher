package com.rrg.virustracker.data

import com.rrg.virustracker.API.CovidAPI
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Repository @Inject constructor(private val covidAPI: CovidAPI) {
/*
    fun getSearchResults(query: String) =
        Pager(
            config = PagingConfig(
                pageSize = 1,
                maxSize = 100,  //limit of items in recycler view
                enablePlaceholders = false //no placeholder
            ),
            pagingSourceFactory = { CovidPagingSource(covidAPI, query) }
        ).liveData*/

    suspend fun getSearchResults_new()  = covidAPI.getAllCountries()

    suspend fun getGlobalData()  = covidAPI.getGlobalData()

    suspend fun getVaccineData() = covidAPI.getVaccineData()

    suspend fun getHistoricalData(country:String) =
        covidAPI.getHistoricalDataByCountry(country)




}