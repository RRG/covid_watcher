package com.rrg.virustracker.data

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize


@Parcelize
class Vaccines(
    val source: String,
    val  data: List<Vaccine>
): Parcelable{

    @Parcelize
    data class Vaccine(
        val candidate: String,
        val mechanism: String,
        val sponsors: List<String>,
        val details: String,
        val institutions: List<String>
    ) : Parcelable
}