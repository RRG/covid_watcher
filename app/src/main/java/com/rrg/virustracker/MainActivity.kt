package com.rrg.virustracker

import android.content.pm.ActivityInfo
import android.content.res.Configuration
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var navController:NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.Theme_CovidWatcher)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navigation_host_main) as NavHostFragment

        navController = navHostFragment.findNavController()


        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_home,
                R.id.navigation_country_details,
                R.id.navigation_vaccine,
                R.id.navigation_app_details
            )
        )
        //setupActionBarWithNavController(navController, appBarConfiguration)

        //quick temp fix for chart issue ...lock orientation for graph fragment
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (destination.id == R.id.countryHistory) {
                val orientation = resources.configuration.orientation
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    // In landscape
                    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                } else {
                    // In portrait
                    requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                }
               // requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            } else if (requestedOrientation != ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR) {
                requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR
            }
        }
        navView.setupWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}