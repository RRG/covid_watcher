package com.rrg.virustracker.Dependancy_inj

import com.rrg.virustracker.API.CovidAPI
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Provides
    @Singleton
    fun  provideRetrofitInstance(): Retrofit =
        Retrofit.Builder()
            .baseUrl(CovidAPI.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Provides
    @Singleton
    fun provideCovid_API_instance(retrofit: Retrofit):CovidAPI =
        retrofit.create(CovidAPI::class.java)

    @ApplicationScope
    @Provides
    @Singleton
    fun provide_app_scope() = CoroutineScope(SupervisorJob())
    //coroutine scope created for as long as the application lives

}

@Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationScope