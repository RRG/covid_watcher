package com.rrg.virustracker.API

import com.rrg.virustracker.BuildConfig
import com.rrg.virustracker.data.Country_data
import com.rrg.virustracker.data.Historical_data
import com.rrg.virustracker.data.Vaccines
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query


interface CovidAPI {

    companion object{
        const val BASE_URL="https://disease.sh/"
        const val API_KEY = BuildConfig.COVID_API_KEY
    }

    //@Header("metadata here","api key :$API_KEY")
    @GET("v3/covid-19/countries/{country}")
    suspend fun searchCountries(
        @Path("country") country: String,
        @Query("strict") fuzzySearch: Boolean?
    ): Country_data


    @GET("v3/covid-19/countries")
    suspend fun getAllCountries(): List<Country_data>

    @GET("v3/covid-19/all")
    suspend fun getGlobalData(): Country_data

    @GET("v3/covid-19/vaccine")
    suspend fun getVaccineData(): Vaccines

    @GET("/v3/covid-19/historical/{country}")
    suspend fun getHistoricalDataByCountry(
        @Path("country") country: String
    ): Historical_data


}