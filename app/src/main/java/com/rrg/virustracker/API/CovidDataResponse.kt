package com.rrg.virustracker.API

import com.rrg.virustracker.data.Country_data

data class CovidDataResponse(
    val results: List<Country_data>
) {
}