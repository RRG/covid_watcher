package com.rrg.virustracker

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp //activate dagger and hilt
class CovidApplicationClass :Application() {
}